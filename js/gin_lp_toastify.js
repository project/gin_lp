/* eslint-disable no-bitwise, no-nested-ternary, no-mutable-exports, comma-dangle, strict */

'use strict';

(($, Drupal, drupalSettings) => {

  Drupal.behaviors.ginLpToastify = {
    attach: (context) => {
      let offset = $( '.ui-dialog-off-canvas' ).length ? $( '.ui-dialog-off-canvas').width() : 0;
      $('.glp-messages--warning', context).once('.glp-messages--warning').each(function(){
        Toastify({
          text: $(this).html(),
          escapeMarkup: false,
          gravity: "bottom",
          duration: 6000,
          position: "right",
          offset: {
            x: 0,
          },
          className:"glp-messages glp-messages--warning",
          backgroundColor:"var(--colorGinWarningBackground)"
        }).showToast();
        $(this).hide();
      });
      $('.glp-messages--error', context).once('.glp-messages--error').each(function(){
        Toastify({
          text: $(this).html(),
          escapeMarkup: false,
          gravity: "bottom",
          duration: 6000,
          position: "right",
          offset: {
            x: offset,
          },
          className:"glp-messages glp-messages--error",
          backgroundColor:"var(--colorGinErrorBackground)"
        }).showToast();
        $(this).hide();
      });
      $('.glp-messages--status', context).once('.glp-messages--status').each(function(){
        if ($(this).parents('.glp-toolbar').length >= 1) {
          return;
        }
        Toastify({
          text: $(this).html(),
          escapeMarkup: false,
          gravity: "bottom",
          duration: 6000,
          position: "right",
          offset: {
            x: offset,
          },
          className:"glp-messages glp-messages--status",
          backgroundColor:"var(--colorGinStatusBackground)"
        }).showToast();
        $(this).hide();
      });

    }
  };

})(jQuery, Drupal, drupalSettings);
