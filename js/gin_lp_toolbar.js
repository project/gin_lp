/* eslint-disable no-bitwise, no-nested-ternary, no-mutable-exports, comma-dangle, strict */

'use strict';

(($, Drupal, drupalSettings) => {

  Drupal.behaviors.ginToolbar = {
    attach: () => {
      $('.glp-configure').add('.glp-toolbar__close').once('gin-toolbar-configure').each((item, elm)=>{
        $(elm).click(()=>{
          $('.glp-toolbar').toggleClass('glp-toolbar--extended glp-toolbar--small');
          calculateWidth();
        })
      });
      const glpToolbar = $('.glp-toolbar');
      $('body').once('gin-toolbar-event').each(()=>{
        const events = ['dialogopen', 'dialogresizestop','dialogresize'];
        events.forEach((eventName)=>{
          $('body').on( eventName, ( event, ui ) => {
            calculateWidth();
          });
        });
        $('body').on( 'dialogclose', ( event, ui ) => {
          const modal = $(event.target);
          if (modal.attr('id') === 'drupal-off-canvas') {
            glpToolbar.css('width', '100%');
          }
        });
      });

      function calculateWidth () {
        if (glpToolbar.hasClass('glp-toolbar--small')) {
          glpToolbar.css('width', '100%');
        } else {
          glpToolbar.css('width', Drupal.behaviors.offCanvas.width + 'px');
        }
      }
      calculateWidth();
    }
  };
  Drupal.toolbar.ToolbarVisualView.prototype.updateToolbarHeight = function () {
    const glpToolbar = $('.glp-toolbar');
    const toolbarTabOuterHeight = $('#toolbar-bar').outerHeight() || 0;
    const toolbarTrayHorizontalOuterHeight = $('.is-active.toolbar-tray-horizontal').outerHeight() || 0;
    const glpToolbarHeight = glpToolbar.outerHeight();
    this.model.set('height', toolbarTabOuterHeight + toolbarTrayHorizontalOuterHeight + glpToolbarHeight);
    const body = $('body')[0];
    body.style.setProperty('padding-top', this.model.get('height') + 'px', 'important');
    glpToolbar.css('top', toolbarTabOuterHeight + toolbarTrayHorizontalOuterHeight);
    glpToolbar.addClass('glp-toolbar--processed');
    this.triggerDisplace();
  }
})(jQuery, Drupal, drupalSettings);

