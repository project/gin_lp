/* eslint-disable no-bitwise, no-nested-ternary, no-mutable-exports, comma-dangle, strict */

'use strict';

(($, Drupal, drupalSettings) => {

  Drupal.behaviors.glp_preview_regions = {
    attach: (context) => {
      if ($('#glp-preview-regions').is(':checked')) {
        $('.layout__region-info').parent().addClass('layout-builder__region');
      };

      $('#glp-preview-regions').once('glp-preview-regions').each(()=>{
        $('#glp-preview-regions', context).change(function (){
          if($(this).is(':checked')){
            $('.layout__region-info').parent().addClass('layout-builder__region');
            $('body').addClass('glp-preview-regions--enable');
          } else {
            $('body').removeClass('glp-preview-regions--enable');
          }
        })
      });

    }
  };

})(jQuery, Drupal, drupalSettings);
