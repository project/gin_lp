# Gin Layout Paragraphs

INTRODUCTION
------------
This module brings the gin admin theme to the layout paragraphs.

INSTALLATION
------------

* Install the Gin Layout Paragraphs module as you would
normally install a contributed
  Drupal module. Visit https://www.drupal.org/node/1897420.

