<?php

namespace Drupal\Tests\gin_lp\Unit\TwigExtension;

use Drupal\Core\Template\Attribute;
use Drupal\gin_lp\TwigExtension\GinLpExtension;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\gin_lp\TwigExtension\GinLpExtension
 * @group gin_lp
 */
class GinLpExtensionTest extends UnitTestCase {

  /**
   * @covers ::calculateDependencies
   */
  public function testGinClasses() {
    $attributes = new Attribute();
    $attributes->addClass('form-item');
    $attributes->addClass('form-item-2');
    $attributes->addClass('js-form-item');

    $cleaned_attributes = GinLpExtension::ginClasses($attributes);
    $this->assertSame('class="glp-form-item glp-form-item-2 js-form-item"', $cleaned_attributes->getClass()->render());
  }

}
