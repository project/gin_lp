const parser = require('postcss-selector-parser');
const fs = require('fs');
const path = require('path');

module.exports = (opts = { }) => {
  return {
    postcssPlugin: 'gin_lp_class_names',
    generate (root, postcss) {
      const ginLpClasses = {}
      const nodeItterator = function(sels) {
        sels.map(function(sel) {
          sel.nodes.forEach((innerNode)=> {

            if (innerNode.type === 'class') {
              if (innerNode.value.indexOf('glp-') === 0) {
                ginLpClasses[innerNode.value] = innerNode.value;
              }
            }
            if (innerNode.type === 'pseudo') {
              innerNode.nodes.forEach((pseudoNode)=> {
                pseudoNode.nodes.forEach((innerPseudoNode)=> {
                  if (innerPseudoNode.value.indexOf('glp-') === 0) {
                    ginLpClasses[innerPseudoNode.value] = innerPseudoNode.value;
                  }
                });
              })
            }
          })
        })
      }
      root.walk((node)=>{
        if (node.selectors != null) {
          return node.selectors.map(function(selector){
            return parser(nodeItterator).process(selector).result
          });
        }
      });
      fs.writeFileSync(opts.targetPath, JSON.stringify(Object.keys(ginLpClasses)));
    },
  }
}
module.exports.postcss = true
