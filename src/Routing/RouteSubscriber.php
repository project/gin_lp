<?php

namespace Drupal\gin_lp\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\gin_lp\Form\GinLpEditComponentForm;
use Drupal\gin_lp\Controller\GinLpChooseComponentController;
use Drupal\gin_lp\Controller\GinLpComponentFormController;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    if ($route = $collection->get('layout_paragraphs.builder.edit_item')) {
      $route->setDefault('_form', GinLpEditComponentForm::class);
    }
    if ($route = $collection->get('layout_paragraphs.builder.choose_component')) {
      $route->setDefault('_controller', GinLpChooseComponentController::class.'::list');
    }
    if ($route = $collection->get('layout_paragraphs.builder.insert')) {
      $route->setDefault('_controller', GinLpComponentFormController::class.'::insertForm');
    }

  }

}
