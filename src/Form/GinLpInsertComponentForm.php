<?php

namespace Drupal\gin_lp\Form;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AfterCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\BeforeCommand;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\layout_paragraphs\Form\InsertComponentForm;
use Drupal\layout_paragraphs\LayoutParagraphsSection;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsTypeInterface;
use Drupal\layout_paragraphs\LayoutParagraphsLayout;
use Drupal\layout_paragraphs\Ajax\LayoutParagraphsEventCommand;

/**
 * Class InsertComponentForm.
 *
 * Builds the form for inserting a new component.
 */
class GinLpInsertComponentForm extends InsertComponentForm {
  /**
   * {@inheritDoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    LayoutParagraphsLayout $layout_paragraphs_layout = NULL,
    ParagraphsTypeInterface $paragraph_type = NULL,
    string $parent_uuid = NULL,
    string $region = NULL,
    string $sibling_uuid = NULL,
    string $placement = NULL
  ) {
    $form = parent::buildForm($form, $form_state, $layout_paragraphs_layout, $paragraph_type, $parent_uuid, $region, $sibling_uuid, $placement);
    $form['wrapper_open'] = [
      '#markup' => '<div class="glp-canvas-form__settings">',
      '#weight' => -1
    ];
    $form['wrapper_close'] = [
      '#markup' => '</div>',
      '#weight' => $form['actions']['#weight'] - 1
    ];
    if (isset($form['behavior_plugins'])) {
      $form['behavior_plugins']['#type'] = 'container';
    }

    $form['actions']['#type'] = 'container';
    $form['actions']['#attributes']['class'][] = 'canvas-form__actions';
    // FIXME: Cancel button causes error
    $form['actions']['cancel']['#access'] = false;
    return $form;
  }

  private function determineSelectedLayout (
    ParagraphInterface $paragraph,
    array &$form,
    FormStateInterface $form_state) {
    $layout_paragraphs_section = new LayoutParagraphsSection($paragraph);
    $path = array_merge($form['#parents'], ['layout']);
    $input_layout_id = NestedArray::getValue($form_state->getUserInput(), $path);
    $layout_id = $input_layout_id ?? $layout_paragraphs_section->getLayoutId();
    // Fixme: for now this is the only layout but is is a hack
    $layout_id = 'layout_multicol_section';
    $layout_id = Html::escape($layout_id);
    return $layout_id;
  }

  /**
   * Form #process callback.
   *
   * Renders the layout paragraphs behavior form for layout selection.
   *
   * @param array $element
   *   The form element.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $form
   *   The complete form array.
   *
   * @return array
   *   The processed element.
   */
  public function layoutParagraphsBehaviorForm(array $element, FormStateInterface $form_state, array &$form) {

    $layout_paragraphs_plugin = $this->paragraphType->getEnabledBehaviorPlugins()['layout_paragraphs'];
    $subform_state = SubformState::createForSubform($element, $form, $form_state);
    if ($layout_paragraphs_plugin_form = $layout_paragraphs_plugin->buildBehaviorForm($this->paragraph, $element, $subform_state)) {

      $selected_layout = $this->determineSelectedLayout($this->paragraph, $element, $subform_state);
      if (empty($selected_layout)) {
        unset($layout_paragraphs_plugin_form['layout']['#default_value']);
        $layout_paragraphs_plugin_form['layout']['#access'] = true;
        $layout_paragraphs_plugin_form['config']['#access'] = false;
      } else {
        // $layout_paragraphs_plugin_form['layout']['#access'] = false; causes the form to submit, don't know why. Visually hide for now.
//        $layout_paragraphs_plugin_form['layout']['#type'] = 'value';
//        $layout_paragraphs_plugin_form['layout']['#value'] = $selected_layout;

//        $layout_paragraphs_plugin_form['layout']['#attributes']['class'][] = 'visually-hidden';
        $layout_paragraphs_plugin_form['layout']['#attributes']['class'][] = 'hidden';
        $layout_paragraphs_plugin_form['config']['#access'] = true;
      }
      $layout_paragraphs_plugin_form['config']['#type'] = 'container';

      $element = $layout_paragraphs_plugin_form;
      // Adjust the Ajax callback to include the entire layout paragraphs form.
      $element_id = Html::getId('layout-paragraphs-element');
      $element['#prefix'] = '<div id="' . $element_id . '">';
      $element['#suffix'] = '</div>';
      $element['layout']['#ajax']['callback'] = [$this, 'ajaxCallback'];
      $element['layout']['#ajax']['wrapper'] = $element_id;
    }
    return $element;
  }

  /**
   * @inheritDoc
   */
  protected function ajaxCloseForm(AjaxResponse &$response) {
    $selector = '#drupal-off-canvas';//Dialog::dialogSelector($this->layoutParagraphsLayout);
    $response->addCommand(new CloseDialogCommand($selector));
  }
}
