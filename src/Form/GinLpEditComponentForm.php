<?php

namespace Drupal\gin_lp\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\layout_paragraphs\Form\EditComponentForm;
use Drupal\layout_paragraphs\LayoutParagraphsLayout;
use Drupal\layout_paragraphs\Ajax\LayoutParagraphsEventCommand;
use Drupal\layout_paragraphs\Utility\Dialog;

/**
 * Class LayoutParagraphsComponentEditForm.
 *
 * Builds the edit form for an existing layout paragraphs paragraph entity.
 */
class GinLpEditComponentForm extends EditComponentForm {



  /**
   * {@inheritDoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    LayoutParagraphsLayout $layout_paragraphs_layout = NULL,
    string $component_uuid = NULL) {
    $form = parent::buildForm($form, $form_state, $layout_paragraphs_layout, $component_uuid);
    $form['wrapper_open'] = [
      '#markup' => '<div class="glp-canvas-form__settings">',
      '#weight' => -1
    ];
    $form['wrapper_close'] = [
      '#markup' => '</div>',
      '#weight' => $form['actions']['#weight'] - 1
    ];
    if (isset($form['behavior_plugins'])) {
      $form['behavior_plugins']['#type'] = 'container';
    }

    $form['actions']['#type'] = 'container';
    $form['actions']['#attributes']['class'][] = 'canvas-form__actions';
    return $form;
  }

  /**
   * Form #process callback.
   *
   * Renders the layout paragraphs behavior form for layout selection.
   *
   * @param array $element
   *   The form element.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $form
   *   The complete form array.
   *
   * @return array
   *   The processed element.
   */
  public function layoutParagraphsBehaviorForm(array $element, FormStateInterface $form_state, array &$form) {

    $layout_paragraphs_plugin = $this->paragraphType->getEnabledBehaviorPlugins()['layout_paragraphs'];
    $subform_state = SubformState::createForSubform($element, $form, $form_state);
    if ($layout_paragraphs_plugin_form = $layout_paragraphs_plugin->buildBehaviorForm($this->paragraph, $element, $subform_state)) {

      $layout_paragraphs_plugin_form['config']['#type'] = 'container';
      $layout_paragraphs_plugin_form['layout']['#access'] = false;

      $element = $layout_paragraphs_plugin_form;
      // Adjust the Ajax callback to include the entire layout paragraphs form.
      $element_id = Html::getId('layout-paragraphs-element');
      $element['#prefix'] = '<div id="' . $element_id . '">';
      $element['#suffix'] = '</div>';
      $element['layout']['#ajax']['callback'] = [$this, 'ajaxCallback'];
      $element['layout']['#ajax']['wrapper'] = $element_id;
    }
    return $element;
  }


  /**
   * @inheritDoc
   */
  protected function ajaxCloseForm(AjaxResponse &$response) {
    $selector = '#drupal-off-canvas';//Dialog::dialogSelector($this->layoutParagraphsLayout);
    $response->addCommand(new CloseDialogCommand($selector));
  }


}
