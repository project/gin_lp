<?php

namespace Drupal\gin_lp\Controller;

use Drupal\Core\Url;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\layout_paragraphs\Controller\ChooseComponentController;
use Drupal\layout_paragraphs\LayoutParagraphsLayout;
use Drupal\layout_paragraphs\LayoutParagraphsLayoutRefreshTrait;
use Drupal\layout_paragraphs\Event\LayoutParagraphsAllowedTypesEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * ChooseComponentController controller class.
 *
 * Returns a list of links for available component types that can
 * be added to a layout region.
 */
class GinLpChooseComponentController extends ChooseComponentController {
  /**
   * Builds the component menu.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The HTTP request.
   * @param \Drupal\layout_paragraphs\LayoutParagraphsLayout $layout_paragraphs_layout
   *   The layout paragraphs layout object.
   *
   * @return array
   *   The build array.
   */
  public function list(Request $request, LayoutParagraphsLayout $layout_paragraphs_layout) {

    $route_name = 'layout_paragraphs.builder.insert';
    $route_params = [
      'layout_paragraphs_layout' => $layout_paragraphs_layout->id(),
    ];
    $query_params = [
      'parent_uuid' => $request->query->get('parent_uuid', NULL),
      'region' => $request->query->get('region', NULL),
      'sibling_uuid' => $request->query->get('sibling_uuid', NULL),
      'placement' => $request->query->get('placement', NULL),
    ];
    $types = $this->getAllowedComponentTypes($layout_paragraphs_layout, $query_params['parent_uuid'], $query_params['region']);
    // If there is only one type to render,
    // return the component form instead of a list of links.
    if (count($types) === 1) {
      $type_name = key($types);
      $type = $this->entityTypeManager()->getStorage('paragraphs_type')->load($type_name);
      $response = $this->formBuilder()->getForm(
        '\Drupal\gin_lp\Form\GinLpInsertComponentForm',
        $layout_paragraphs_layout,
        $type,
        $query_params['parent_uuid'],
        $query_params['region'],
        $query_params['sibling_uuid'],
        $query_params['placement']
      );
      return $response;
    }

    foreach ($types as &$type) {
      $url_route_params = $route_params + ['paragraph_type' => $type['id']];
      $url_options = ['query' => $query_params];
      $type['url'] = Url::fromRoute($route_name, $url_route_params, $url_options)->toString();
      $type['link_attributes'] = new Attribute([
        'class' => ['use-ajax'],
      ]);
    }

    $section_components = array_filter($types, function ($type) {
      return $type['is_section'] === TRUE;
    });
    $content_components = array_filter($types, function ($type) {
      return $type['is_section'] === FALSE;
    });
    $component_menu = [
      '#title' => $this->t('Choose a component'),
      '#theme' => 'layout_paragraphs_builder_component_menu',
      '#attributes' => [
        'class' => ['lpb-component-list'],
      ],
      '#types' => [
        'layout' => $section_components,
        'content' => $content_components,
      ],
      '#attached' => [
        'library' => [
          'layout_paragraphs/component_list',
        ],
      ],
    ];
    return $component_menu;

  }

}
