<?php

namespace Drupal\gin_lp\Element;

use Drupal\Core\Url;
use Drupal\Core\Render\Markup;
use Drupal\Core\Render\Renderer;
use Drupal\Component\Utility\Html;
use Drupal\Component\Serialization\Json;
use Drupal\layout_paragraphs\Element\LayoutParagraphsBuilder;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Layout\LayoutPluginManager;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\Core\Render\Element\RenderElement;
use Drupal\layout_paragraphs\Utility\Dialog;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\layout_paragraphs\LayoutParagraphsSection;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\layout_paragraphs\LayoutParagraphsComponent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\layout_paragraphs\LayoutParagraphsLayoutTempstoreRepository;

/**
 * Overrites a render element for building the Layout Builder UI.
 *
 */
class GinLpLayoutParagraphsBuilder extends LayoutParagraphsBuilder {

  /**
   * Returns the render array for a insert component button.
   *
   * @param array[] $route_params
   *   The route parameters for the link.
   * @param array[] $query_params
   *   The query paramaters for the link.
   * @param int $weight
   *   The weight of the button element.
   * @param array[] $classes
   *   A list of classes to append to the container.
   *
   * @return array
   *   The render array.
   */
  protected function insertComponentButton(array $route_params = [], array $query_params = [], int $weight = 0, array $classes = []) {
    $command = parent::insertComponentButton($route_params, $query_params, $weight, $classes);
    $command['#attributes']['data-dialog-renderer'] = 'off_canvas';
    if (isset($command['#attributes']['data-dialog-options'])) {
      $options = Json::decode($command['#attributes']['data-dialog-options']);
      $options['dialogClass'] = $options['dialogClass'] . ' ui-dialog-off-canvas';
      $command['#attributes']['data-dialog-options'] = Json::encode($options);
    }
    return $command;
  }

  /**
   * Returns the render array for a create section button.
   *
   * @param array[] $route_params
   *   The route parameters for the link.
   * @param array[] $query_params
   *   The query parameters for the link.
   * @param int $weight
   *   The weight of the button element.
   * @param array[] $classes
   *   A list of classes to append to the container.
   *
   * @return array
   *   The render array.
   */
  protected function insertSectionButton(array $route_params = [], array $query_params = [], int $weight = 0, array $classes = []) {
    $command = parent::insertSectionButton($route_params, $query_params, $weight, $classes);
    $command['#attributes']['data-dialog-renderer'] = 'off_canvas';
    $command['#attributes']['data-dialog-options'] = Json::encode(['width' => 886]);
    return $command;
  }

}
